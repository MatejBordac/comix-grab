#####
# Exception details are written into file specified in ERROR_LOG_FILE constant.
#
# It's not working 100%, I'm working on making it better.
#
# More features to come.
#####

# encoding: utf-8

require 'net/http'
require 'open-uri/cached'

require 'nokogiri'
require 'zip'
#require 'json'

#require 'pry'

module ComixGrab

  OpenURI::Cache.cache_path = '.cache'

  attr_accessor :make_zip, :sleepy, :delay, :size_conv, :comix_str

  TO_KB = 0.0009765625
  TO_MB = 0.0000009765625
  TO_B = 1
  #SAVE_FILE = "save"
  ERROR_LOG_FILE = "errors"

  class DownloadProcess

    def initialize(comix_str = nil)
      # setup connection
      host = "bato.to"
      port = 80
      @http = Net::HTTP.new(host, port)

      @chapter_names = []
      @chapter_links = []
      @title = ""

      # options default values
      @silent = false
      @sleepy = false
      @delay = 0.5
      @make_zip = true
      @size_conv = TO_MB
      @comix_str = nil
      @dl_path = "./"

      init(comix_str) if comix_str
    end

    def log(msg, level)
      case level
      when 1
        puts msg unless @silent
      when 2
        puts msg
      when 3
        File.open(ERROR_LOG_FILE, "a") do |f|
          f.write msg
          f.write "\n"
        end
      end
      # add more levels for more options

    end

    # to log exceptions easier
    def logex(exception)
      log(Time.now, 3)
      log(exception.message, 3)
      log(exception.cause, 3)
      log(exception.backtrace.inspect, 3)
    end

    def init(comix_str)
      log("Recieved manga string: #{comix_str}", 1)

      path = "/comic/_/comics/#{comix_str}/"
      link_match = "http://bato.to/read/_/"
      data = ""

      # try to get data
      log("Fetching data..", 1)
      begin
        result = @http.get(path)
      rescue Exception => e
        log("init: Connection error", 2)
        logex(e)
        return false
      end

      # check for http response code
      if result.code == "200"
        data = result.body
      else
        log(result.code + ": " + result.message, 2)
        log("Maybe invalid manga string", 2)
        return false
      end

      @comix_str = comix_str
      @dl_count = 0
      @latest_dl = [nil, nil]

      log("Success.", 1)
      log("Now parsing..", 1)

      # parse HTML data
      begin
        http_doc = Nokogiri::HTML(data)
      rescue Exception => e
        log("init: Nokogiri error", 2)
        logex(e)
        return false
      end

      # create structure for more searches
      data = "<root>"
      begin
        data << http_doc.xpath("//tr[@class='row lang_English chapter_row']").to_html
      rescue Exception => e
        log("init: Parsing error.", 2)
        logex(e)
        return false
      end
      data << "</root>"
      data.gsub!(/\&/, '')

      begin
        @title = http_doc.xpath("//h1[@class='ipsType_pagetitle']").first.inner_html
      rescue Exception => e
        log("init: Title parsing error.", 2)
        logex(e)
        return false
      end

      # remove invalid file name characters
      @title.gsub!(/[\x00\/\\:\*\?\"<>\|]/, '')
      @title.strip!

      begin
        xml_doc = Nokogiri::XML(data)
      rescue Exception => e
        log("init: Error making XML doc.", 2)
        logex(e)
        return false
      end

      @chapter_names = []
      @chapter_links = []

      # find chapter names and http links
      begin
        xml_doc.xpath("//a").each do |n|
          link = n.attribute("href").value
          if link.include? link_match
            @chapter_names.push n.content.strip
            @chapter_links.push link
          end
        end
      rescue Exception => e
        log("init: XML parsing error.", 2)
        logex(e)
        return false
      end

      if @chapter_names.empty?
        log("Didn't find any chapters.", 2)
        return false
      else
        log("Found #{@chapter_names.size} chapters", 1)
      end

      @chapter_names.reverse!
      @chapter_links.reverse!

      @chapter_names.each do |el|
        el.gsub!(/[\x00\/\\:\*\?\"<>\|]/, '')
        while el.end_with?(".")
          el.chop!
        end
      end

      return true
    end

    def get_chapter_page_count(number)
      # checks, in case method is called outside of dl_chapter()
      if @chapter_names.empty?
        log("pgCount: Manga data empty", 2)
        return 0
      end

      if number > @chapter_names.size || number <= 0
        log("pgCount: Given invalid chapter number.", 2)
        log("Valid range: 1 - #{@chapter_names.size}", 1)
        return 0
      end

      # ready the chapter link
      link = @chapter_links[number - 1]
      uri = URI(link)
      data = ""

      # try to get html page
      begin
        result = @http.get(uri.path)
      rescue Exception => e
        log("dl_chapter: Connection error while getting chapter #{number}.", 2)
        logex(e)
        return 0
      end

      if result.code == "200"
        data = result.body
      else
        log("HTML response error.", 2)
        log(result.code + ": " + result.message, 2)
        return 0
      end
      begin
        html_doc = Nokogiri::HTML(data)
      rescue Exception => e
        log("pgCount: Nokogiri error.", 2)
        logex(e)
        return 0
      end

      # find out page count
      search_str = "//select[@id='page_select']"
      begin
        ret = html_doc.xpath(search_str).first.element_children.size
      rescue Exception => e
        log("pgCount: HTML parsing error.", 2)
        logex(e)
        return 0
      end

      log("Found #{ret} pages for chapter id #{number}.", 1)

      return ret

    end

    def dl_chapter(number)
      # validity checks
      if @chapter_names.empty?
        log("dl_chapter: Manga data empty", 2)
        return false
      end

      if number > @chapter_names.size || number <= 0
        log("dl_chapter: Given invalid chapter number.", 2)
        log("Valid range: 1 - #{@chapter_names.size}", 1)
        return false
      end

      # chapter specific data
      name = @chapter_names[number - 1]
      link = @chapter_links[number - 1]
      pages = get_chapter_page_count(number)
      file_names = []

      # try to create folder structure
      if File.exists? @title
        log("Manga dir exists, going in.", 1)
      else
        begin
          Dir.mkdir(@title)
        rescue Exception => e
          log("dl_chapter: Error making dir '#{@title}'", 2)
          logex(e)
          return false
        end
        log("Created manga dir #{@title}", 1)
      end

      dl_path = @dl_path + @title + "/"

      if File.exists?(dl_path + name)
        log("dl_chapter: Dir with chapter name #{name} already exists", 2)
        return false # TODO this may be overkill
      end

      dl_path = dl_path + name + "/"
      begin
        Dir.mkdir(dl_path)
      rescue Exception => e
        log("dl_chapter: Error making dir '#{dl_path}'", 2)
        logex(e)
        return false
      end

      log("Created chapter dir '#{name}'", 1)
      log("Downloading chapter with id #{number}..", 1)

      # don't zip if not all pages were downloaded
      dl_error = false

      # try to download all pages of chapter
      for i in 1..pages do
        search_str = "/html/body/div/div/div/div/div/div/img"
        doc = ""
        begin
          open(link + "/#{i}") do |io|
            doc = Nokogiri::HTML(io.read)
          end
        rescue Exception => e
          log("chapterDL: Error while getting page #{i}, skipping.", 2)
          logex(e)
          dl_error = true
          next
        end

        begin
          pic_link = doc.search(search_str).first["src"]
        rescue Exception => e
          log("chapterDl: Error getting link for picture #{i}, skipping.", 1)
          logex(e)
          dl_error = true
          next
        end

        from = pic_link.rindex("/") + 1
        to = pic_link.size - 1
        fn = pic_link[from..to]
        file_names.push(dl_path + fn)
        fn = dl_path + fn

        log("Page #{i}/#{pages}", 1)

        begin
          open(pic_link) do |f|
            File.open(fn, 'wb') do |file|
              file.puts f.read
            end
          end
        rescue Exception => e
          log("chapterDl: Error getting/writing page #{i}, skipping.", 2)
          logex(e)
          dl_error = true
          next
        end
        # to make a coffee between downloads
        sleep @delay
      end

      if @make_zip && !dl_error
        log("Making zip file..", 1)
        #folder = Dir.pwd
        zipfn = @dl_path + @title + "/" + name + ".zip"

        # attempt to zip all files if they were downloaded
        begin
          Zip::File.open(zipfn, Zip::File::CREATE) do |zf|
            file_names.each do |f|
              from = fn.rindex("/") + 1
              to = f.size - 1
              full_path = f
              full_path = (File.expand_path(File.dirname(__FILE__)) + "/" + f) if f[0] == "."
              zf.add(f[from..to], full_path) if File.file? f
            end
          end
        rescue Exception => e
          log("chapterDl: Error making zip file '#{zipfn}'", 2)
          logex(e)
          return false
        end

        # print zip file info
        log("File made: " + name + ".zip", 1)
        size = (File.size?(zipfn) * @size_conv).round(1)
        size_name = ""
        case @size_conv
        when TO_KB
          size_name = "KB"
        when TO_MB
          size_name = "MB"
        when TO_B
          size_name = "B"
        end
        log("Size: #{size} #{size_name}", 1)

        # delete files that were zipped
        log("Deleting original files..", 1)
        file_names.each do |f|
          begin
            File.delete(f) if File.file? f
          rescue Exception => e
            log("chapterDl: Error deleting file '#{fn}'", 2)
            logex(e)
          end
        end

        begin
          Dir.rmdir(dl_path)
        rescue
          log("Error deleting folder #{dl_path}", 2)
          logex(e)
        end
      end

      # TODO for progress saving, not yet implemented
      if @latest_dl[0].to_i < number
        @latest_dl[0] = number
        @latest_dl[1] = @chapter_names[number - 1]
      end

      log("Done.", 1)
      return true

    end

    # param is int array of chapter numbers to list
    def list_chapters(idArray)
      ctr = 0
      log("Listing chapters for #{@title}", 2)
      idArray.each do |e|
        break if e > @chapter_names.size
        chN = @chapter_names[e - 1]
        log("id:#{e}: #{chN}", 2)
        ctr += 1
      end
      log("Listed #{ctr} chapters.", 2)
    end

    # param is int array of chapter numbers to download
    def dl_chapters(idArray)
      ctr = 0
      idArray.each do |e|
        break if e > @chapter_names.size
        dl_chapter(e)
        ctr += 1
        log("Downloaded chapter '#{@chapter_names[e - 1]}' from '#{@title}'", 1)
      end
      log("Downloaded #{ctr} chapters of #{@title}", 1)

      # TODO for saving, not yet implemented
      @dl_count += ctr

      return true
    end

  end

end

