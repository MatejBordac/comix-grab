# encoding: UTF-8

require 'rubygems'
require 'bundler/setup'

require 'clipboard'

require './src/core.rb'
require 'pry'

## TODO modify chapter names with regex

MAX = 1000
SIZES = [ComixGrab::TO_B, ComixGrab::TO_KB, ComixGrab::TO_MB]


def get_num_array(str)
  arr = str.split(",")
  ret = []
	if arr.include?("all")
		for i in 1..MAX do
			ret.push(i)
		end
		return ret
	else
		arr.each do |e|
			if e.include? "-"
				s = e.split("-")
				return "WRONG_ARG" if s.size !=2
				s[0] = s[0].to_i
				s[1] = s[1].to_i
				return "WRONG_ARG" if s[0] == 0 || s[1] == 0
				return "WRONG_ARG" if s[0] > s[1]
				for i in s[0]..s[1] do
					ret.push(i)
				end
			else
				return "WRONG_ARG" if e.to_i == 0
				ret.push(e.to_i)
			end
		end
		return ret
	end
end

def arg_to_value(arg)
	case arg.downcase
	when "on"
		return true
	when "off"
		return false
	when "mb"
		return ComixGrab::TO_MB
	when "kb"
		return ComixGrab::TO_KB
	when "b"
		return ComixGrab::TO_B
	end
	if arg.to_f > 0
		return arg.to_f
	end

	return "WRONG_ARG"
end

def parse_args(cmd, arg)
	# cmdList = ["list", "dl", "sleep", "zip", "size"]

	arr = arg.split(" ")

	case cmd
	when "zip"
		return "WRONG_ARG" if arr.size != 1
		val = arg_to_value(arr[0])
		return val if (val == true || val == false)
	when "size"
		return "WRONG_ARG" if arr.size != 1
		val = arg_to_value(arr[0])
		return val if (SIZES.include?(val))
	when "sleep"
		if arr.size == 1
			val = arg_to_value(arr[0])
			return val if val == true || val == false
		elsif arr.size == 2
			val = arg_to_value(arr[0])
			time = arg_to_value(arr[1])
			if (val == true || val == false) && (time.is_a? Float)
				return [val, time]
			end
		end
		return "WRONG_ARG"
	when "list"
		return get_num_array(arg.delete(" "))
	when "dl"
		return get_num_array(arg.delete(" "))
	when "dlb"
		return get_num_array(arg.delete(" "))
	end

	return "WRONG_ARG"
end

def print_help(sz, slp, zip)
	help = "list {1,2,3-10,all}
  - lists available chapters with their id
dl {1,2,3-10,all}
  - downloads chapters with specified id
size {MB/KB/B}
  - changes size format to MB, KB or B,
    currently: #{sz}
sleep {on/off} {float}
  - adds 'float' seconds delay between downloads if turned on,
    currently: #{slp}, delay: #{@delay} seconds
zip {on/off}
  - puts each downloaded chapter into single zip, deleting pictures
    currently: #{zip}
new
  - set new manga string
status
  - NYI: show status of background downloads
help
  - displays this list
Press ctrl + C or input q to quit.\n"

	puts help
end




puts "##############################"
puts "###    Comix downloader    ###"
puts "##############################"
puts
puts "*** Step 1: Copy and paste search string from url ***"
puts "Example : nisekoi-r951"
puts "Type q to quit, clp to paste from clipboard."

$main_process = ComixGrab::DownloadProcess.new(false)
$bg_processes = []

while true
	print "> "
	input = gets.strip
	exit if input == 'q'
	input = Clipboard.paste if input == 'clp'
	# break if $main_process.init(URI::encode(input))
	break if $main_process.init(input)
end

sz = "MB"
slp = "off"
zip = "on"

puts

puts "*** Step 2: Choose option ***"
print_help(sz, slp, zip)



cmd = ""
while true
	print "> "

	cmd = gets.strip.split(" ", 2)

  #debugging
  if cmd[0] == "pry"
    binding.pry
    next
  end
  ###

  if cmd[0] == "save"
    $main_process.saveData
    next
  end

	if cmd[0] == "help"
		print_help(sz, slp, zip)
		next
	end

	break if cmd[0] == "q"

	if cmd[0] == "new"
		puts "Enter new manga string (q to quit, r to return, clp to paste):"
		while true
			print "> "
			input = gets.strip
			exit if input == 'q'
			break if input == 'r'
			input = Clipboard.paste if input == 'clp'
			break if $main_process.init(input)
		end
		next
	end

	if cmd[0] == "status"
		# dl status of background operations
		next
	end

	if cmd.size < 2
		puts "Invalid/incomplete command."
		next
	end


	ret = parse_args(cmd[0], cmd[1])
	if ret == "WRONG_ARG"
		puts "Invalid input."
		next
	end


	case cmd[0]
	when "list"
		$main_process.list_chapters(ret)
	when "dl"
		$main_process.dl_chapters(ret)
	when "dlb"
		dl = ComixGrab::DownloadProcess.new(true)
		dl.init($main_process.comix_str)
		dl.make_zip = $main_process.make_zip
		dl.sleepy = $main_process.sleepy
		dl.delay = $main_process.delay
		dl.size_conv = $main_process.size_conv
		t = Thread.new do
			dl.dl_chapters(ret)
			puts "Finished background proccess, #{$bg_processes.size - 1} left."
		end
		$bg_processes.push t
		puts "Started download in background.."
	when "size"
		$main_process.size_conv = ret
		puts "Size format set to #{cmd[1]}."
		sz = cmd[1]
	when "sleep"
		$main_process.sleepy = ret[0]
		$main_process.delay = ret[1] if ret[1] != nil
		puts "Sleep config set to #{cmd[1]}."
		puts "Delay is set #{@delay} seconds." if ret[1] != nil
		slp = cmd[1]
	when "zip"
		$main_process.make_zip = ret
		puts "Zip config set to: #{cmd[1]}."
		zip = cmd[1]
	else
		puts "Invalid command."
	end

end

if !$bg_processes.empty?
	puts "Waiting for background downloads to finish.."
	$bg_processes.each do |p|
		p.join
	end
	puts "Done."
end
